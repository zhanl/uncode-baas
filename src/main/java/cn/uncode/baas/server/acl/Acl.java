package cn.uncode.baas.server.acl;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import cn.uncode.dal.utils.JsonUtils;

public class Acl implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 2135756349801970386L;

    private String group;

    private String role;

    private String user;

    public String getGroup() {
        return group;
    }

    public List<String> getGroups() {
        if (StringUtils.isNotBlank(group)) {
            return Arrays.asList(group.split(","));
        }
        return null;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getRole() {
        return role;
    }

    public List<String> getRoles() {
        if (StringUtils.isNotBlank(role)) {
            return Arrays.asList(role.split(","));
        }
        return null;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUser() {
        return user;
    }

    public List<String> getUsers() {
        if (StringUtils.isNotBlank(user)) {
            return Arrays.asList(user.split(","));
        }
        return null;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void fromJson(String value) {
        if (StringUtils.isNotBlank(value)) {
            Map<?, ?> valueMap = JsonUtils.fromJson(value, Map.class);
            fromMap(valueMap);
        }
    }

    public void fromMap(Map<?, ?> valueMap) {
        if (valueMap.containsKey("group")) {
            this.group = String.valueOf(valueMap.get("group"));
        }
        if (valueMap.containsKey("role")) {
            this.role = String.valueOf(valueMap.get("role"));
        }
        if (valueMap.containsKey("user")) {
            this.user = String.valueOf(valueMap.get("user"));
        }
    }

    public String toJson() {
        return JsonUtils.objToJson(this);
    }

    public String toString() {
        return JsonUtils.objToJson(this);
    }

    public String getStringValue() {
        StringBuffer sb = new StringBuffer();
        if (StringUtils.isNotBlank(group)) {
            sb.append(group);
        }
        if (StringUtils.isNotBlank(role)) {
            if (sb.length() > 0) {
                sb.append(",");
            }
            sb.append(role);
        }
        if (StringUtils.isNotBlank(user)) {
            if (sb.length() > 0) {
                sb.append(",");
            }
            sb.append(user);
        }
        return sb.toString();
    }

}
